Format for this file is:
  designation in Osmond PCB, part type, value, count


Capacitors
----------

| | Type | Value | Count |
| :--------| :------| :-----| :-----|
| cpoly102 | Mylar polyester | 1nF | 5
| cpoly472 | Mylar polyester | 4.7nF | 2
| cpoly104 | Mylar polyester | 100nF | 5
| cel1 | Electrolytic capacitor | 10µF | 2
| | | | __14__ |

Resistors
---------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| r3 | Carbon film 1/4W | 470 | 2
| r3 | Carbon film 1/4W | 4.7K | 4
| r3 | Carbon film 1/4W | 10K | 10
| r3 | Carbon film 1/4W | 22K | 5
| r3 | Carbon film 1/4W | 47K | 1
| r3 | Carbon film 1/4W | 100K | 6
| r3 | Carbon film 1/4W | 220K | 13
| r3 | Carbon film 1/4W | 470K | 2
| r3 | Carbon film 1/4W | 1M (or 470K) | 1
| r3 | Carbon film 1/4W | 2.2M | 3
| r3 | Carbon film 1/4W | X (10K)| 1
| r3 | Carbon film 1/4W | Y (10K)| 1
| r3 | Carbon film 1/4W | CHAO (220K)| 1
| r3 | Jumper wire | 0 | 1
| | | | __51__

_Notes: X and Y are recommended 10K but you could try other values. CHAO is recommended 220K but you could try other values. The 1M resistor is recommended to change to 470K as this improves the response of the Delay pot. These values are mostly down to Richard Brewster's documentation._

Diodes
------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| do3 | Signal diode | 1n914 | 2
||||__2__


Transistors
-----------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| qpnp | PNP transistor | BC557B | 11
| qnpn | NPN transistor | BC547B | 4
||||__15__

_Note: PNP can also be BC556 or BC558, and NPN can also be BC546 or BC548._

Semiconductor ICs
-----------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| dip14 |  JFET input quad opamp | TL064 | 1
| dip16 |  Dual operational transconductance amps | LM13700 | 1
||||__2__

Potentiometers
--------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wnob | Linear potentiometer | 10K | 4
||||__4__

_Note: I mostly prefer 10K vs 100K (both tested) but you could try other values too. I actually prefer 100K on the Resonance pot though._


Power input
-----------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wobelisk | battery clip | 9v | 1
||||__1__

Outputs
-------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wpod| Mono audio jack | 3.5mm or 1/4" | 1
||||__1__

_Note: You can also use wsquin and wsquisquin as audio outs (for a low pass and high pass sound - wpod is band pass) or even rig up some kind of mixer if you wanted._

Connections (no parts, possibly banana connectors etc.)
--------------------------------------------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wtrin  || Touch point or control input | 1
| wttrchaos||Touch point or control input| 1
| wtttrrugg||Touch point or control input|1
| wsqin ||Touch points or control inputs | 1
| wsquin||Audio/Control output| 1
| wsquisquin||Audio/Control output| 1
||||__6__

_Note: wtttrrugg works well as a touch point, triggering pulses, and can also be connected to other sources (such as Rollz-5). wttrchaos and wtrin can also be touch points but don't do a lot. More useful when connected to other sources (such as Rollz-5). wsqin is weird and it's unclear how to connect this. Richard Brewster uses a piezo but I didn't find this very useful (it had a small effect but not very playable/gestrual). In other projects (such as Tetrassi) it's looking like a button but that did nothing when I tested it. Most useful was wiring as 2 separate control inputs (again hooked up to Rollz-5 in my testing). wsquin and wsquisquin can be used for audio out but can also be patched to other points (this can also be done through touch points). I don't feel like that's very clear. Personally, I wire everything to 3.5mm jack sockets and can patch things into one another etc. If you're using bananas or touch points you'll need to decide what is an audio jack and what is a banana/touch._
