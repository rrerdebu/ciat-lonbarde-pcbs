Format for this file is:
  designation in Osmond PCB, part type, value, count

Capacitors
----------

| | Type | Value | Count |
| :--------| :------| :-----| :-----|
| cpoly102 | Mylar polyester | 1nF | 4
| c104 | Poly or Ceramic MLCC | 100nF | 9
| c105 | Poly or Ceramic MLCC | 1µF | 10
| cel2 | Electrolytic capacitor | 470µF | 3
| | | | __26__ |

Resistors
---------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| r3 | Carbon film 1/4W | 4.7K | 8
| r3 | Carbon film 1/4W | 10K | 30
| r3 | Carbon film 1/4W | 22K | 16
| r3 | Carbon film 1/4W | 100K | 24
| r3 | Carbon film 1/4W | 470K | 20
| r3 | Carbon film 1/4W | 10M | 4
| | | | __102__

Diodes
------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| do3 | Signal diode | 1n914 | 9
||||__9__

Transistors
-----------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| qpnp | PNP transistor | BC557B | 14
| qnpn | NPN transistor | BC547B | 18
||||__32__

_Note: PNP can also be BC556 or BC558, and NPN can also be BC546 or BC548._

Semiconductor ICs
-----------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| dip8 |  Audio power amplifier | LM386 | 2
| dip14 |  JFET input quad opamp | TL064 | 4
| dip14 |  Quad opamp | LM324 | 2
| dip14 |  Quad bilateral switch | CD4066 | 1
||||__9__

Switches
-------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| chairg | G Switch | SPST switch | 2
||||__2__

_Note: Pretty much any switch should do here. I used simple on-off SPST switches (with 2 pins). You could use momentary buttons etc. too._

Potentiometers
--------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wnob | Linear potentiometer | 100K | 2
||||__2__

_Note: Only wire the two outer lugs (see wtrin connections below for centre lugs). I used 100K but 10K should also be good._

Power input
-----------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| U61 | battery clip | 9v | 1
||||__1__

Outputs
-------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wsquin| Speaker connector | Wire to a speaker | 2
||||__2__

_Note: There are two speaker outputs. If you want to wire to a jack socket (and I do) you should use a stereo socket. One output round hole to tip, the other output round hole to ring, and connect both speaker square holes together and wire to the sleeve connector of the jack. You could wire to two separate mono outputs. If you want to just wire everything together to a mono socket then you have two choices. Either wire the two square holes together and connect to the jack sleeve/ground, and connect both round outputs together and connect to the jack tip connector. Or, use summing resistors - e.g., wire one round out to a 1K resistor, wire the other round out to a 1K resistor, connect the other ends of both resistors together and then connect to the tip of the jack socket. The square outputs are just connected together and to sleeve/ground as before. Use of summing resistors is a little quieter and cleaner. Without summing resistors it's a little more wild/noisy. I prefer without summing resistors if I had to but would choose stereo over mono any day of the week. Also note, these outputs are all coming out of LM386 chips designed to drive speakers and won't do any favours for audio quality on jack sockets. I haven't figured out if you can bypass them yet (they seem to be using both inverting and non-inverting inputs which is beyond my knowledge right now)._

Connections (no parts, possibly banana connectors etc.)
--------------------------------------------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wtrin  || Wire the centre lug of your potentiometers here | 2
| wsqout|There are 2 of each on each 'half' of the board|Wire each pair together| 4
| 4squin|There are 2 of each on each 'half' of the board|Wire each pair together| 4
| bigvia|Internal board connections|No parts| 22
||||__32__

_Note: I've tried patching these with a couple of paper rollz circuits but results weren't that impressive. If you do patch, I'd wire both wires of each pair per patch input, rather than have these connections patchable themselves. If you wanted to really get into it you could wire via two lugs of a switched jack so that the connection breaks when patched. YMMV._
