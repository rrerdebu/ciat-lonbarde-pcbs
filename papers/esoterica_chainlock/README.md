# Esoterica Chainlock

## esoterica_chainlock

![Esoterica Chainlock](esoterica_chainlock.png)

As per my usual, I've made any pads/holes that I'd use hookup wire for (rather than just stick parts in) larger to accommodate 22AWG wire.

Also, the Osmond file complains about errors. Some of the trace routing isn't 'technically' sound according to Osmond though 'artistically' it probably works the same. At least one component is doubled and overlaid which also create technical conflicts (and mucks up the BOM) in the software. I don't believe these will affect the end result PCB. I have ordered PCBs and will update when tested. Also note that the BOM is incorrect. Because of the duplicate parts the part counts are doubled. I'm working through corrections and will update in due course.

I have built it and it works fine (though I want to confirm the tidied version and leave this alone in due course).

I forgot to note what I used for X/Y resistors and will update when I can. Notes and documentation are all updated though and this should be considered proven working.

## esoterica_chainlock_tidy

![Esoterica Chainlock Tidy](esoterica_chainlock_tidy.png)

As per my usual, I've made any pads/holes that I'd use hookup wire for (rather than just stick parts in) larger to accommodate 22AWG wire.

This is the version where I'm working through corrections. I have removed all duplicate parts and recreated any traces that got broken in the process. I compared with the original to make sure I haven't missed anything but would benefit from another going over and another set of eyes. Osmond validates the file as having no errors (though that's not a guarantee of wrong/missing trace) which is a positive step. I didn't start looking into this until after I order some boards from the original (above). I'll confirm if this works in due course. The BOM/parts count should be okay though (except missing mods, X/Y values, connection details as I've not tried a built yet).
